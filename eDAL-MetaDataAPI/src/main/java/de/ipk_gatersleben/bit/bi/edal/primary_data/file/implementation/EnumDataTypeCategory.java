/**
 * Copyright (c) 2023 Leibniz Institute of Plant Genetics and Crop Plant Research (IPK), Gatersleben, Germany.
 *
 * We have chosen to apply the GNU General Public License (GPL) Version 3 (https://www.gnu.org/licenses/gpl-3.0.html)
 * to the copyrightable parts of e!DAL, which are the source code, the executable software, the training and
 * documentation material. This means, you must give appropriate credit, provide a link to the license, and indicate
 * if changes were made. You are free to copy and redistribute e!DAL in any medium or format. You are also free to
 * adapt, remix, transform, and build upon e!DAL for any purpose, even commercially.
 *
 *  Contributors:
 *       Leibniz Institute of Plant Genetics and Crop Plant Research (IPK), Gatersleben, Germany
 */
package de.ipk_gatersleben.bit.bi.edal.primary_data.file.implementation;

/*
 * Enumeration with names and related synonyms for the classification of a species.
 */
public enum EnumDataTypeCategory {
	
	PHENOTYPINGSTUDY(new DataTypeCategory("Phenotyping study",
			new String[] { "phenotypic", "phenomics", "high-throughput phenotyping (HTP)","high-throughput phenotyping","phenotyping","imaging" })),
	
	GENOMEANNOTATION(new DataTypeCategory("Genome annotation",
			new String[] { "Genome annotation", "genome annotation"})),
	
	GENEANNOTATION(new DataTypeCategory("Gene annotation",
			new String[] { "Gene annotation", "gene annotation"})),
	
	GENETICMAP(new DataTypeCategory("Genetic map",
			new String[] { "Genetic map", "genetic map"})),
	
	GENOTYPINGEXPERIMENT(new DataTypeCategory("Genotyping Experiment",
			new String[] { "Genotyping Experiment", "Genotyping", "genotyping" })),
	
	PATHWAY(new DataTypeCategory("Pathway",
			new String[] { "Pathway", "pathway","path way", "Path way" })),
	
	SEQUENCINGEXPERIMENT(new DataTypeCategory("Sequencing Experiment",
			new String[] { "Sequencing Experiment", "Sequencing", "sequencing", "Sequences", "sequences" }));

	EnumDataTypeCategory(DataTypeCategory dataTypeCategory) {
			this.dataTypeCategory=dataTypeCategory;	
	}

	public DataTypeCategory value() {
		return dataTypeCategory;
	}

	DataTypeCategory dataTypeCategory;

}